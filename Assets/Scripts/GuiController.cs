﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiController : MonoBehaviour
{
    public GameObject Panel;
    public Image BackImage;
    public Text ScoreTxt, BestScoreTxt;


    public AnimationCurve PanelCurve, ColorCurve;

    public void GameOver()
    {

        if (LevelController.Instance.PlatfromCount > Profile.Instance.Score)
        {
            BestScoreTxt.text = "NEW BEST SCORE : " + LevelController.Instance.PlatfromCount;
            Profile.Instance.Score = LevelController.Instance.PlatfromCount;
        }
        else
        {
            BestScoreTxt.text = "BEST SCORE : " + Profile.Instance.Score;
            ScoreTxt.text = "Score: " + LevelController.Instance.PlatfromCount;
        }
        StartCoroutine(ColorShow());
        StartCoroutine(ShowPanel());
    }

    IEnumerator ShowPanel()
    {
        float time = 0;

        while (time < 1)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            Panel.transform.localPosition = new Vector3(PanelCurve.Evaluate(time)*1000, 0, 0);
            time += Time.deltaTime;
        }
    }

    IEnumerator ColorShow()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime;
            BackImage.color = new Color(0, 0, 0, ColorCurve.Evaluate(time));
            yield return new WaitForSeconds(Time.deltaTime/5);
        }
    }

    public void Restart()
    {
        Application.LoadLevel(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    static GuiController instance;
    public static GuiController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GuiController>();
            }
            return instance;
        }
    }
}
