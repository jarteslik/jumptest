﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{

    public AnimationCurve CurveSquish, CurveUnSquish;
    bool jump = false;
    Rigidbody2D charBody;

    // Start is called before the first frame update
    void Start()
    {
        charBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !jump)
        {
            StartSquish();
        }
        if (charBody.velocity.y == 0)
        {
            jump = false;
            tag = "Char/Stay";
        }
        if (charBody.velocity.y < 0 && jump)
        {
            tag = "Char/Fall";
        }
    }
    public void StartSquish()
    {
        StartCoroutine(Squish());
    }

    void Jump(float force)
    {
        charBody.AddForce(new Vector2(0, force * 50), ForceMode2D.Impulse);
        jump = true;
    }

    IEnumerator Squish() //сжимаем
    {
        float time = 0;
        Vector2 startSize = transform.localScale;
        while (Input.GetMouseButton(0))
        {

            time += Time.deltaTime / 4;
            if (time < 0.25)
                transform.localScale = new Vector3(startSize.x + (CurveSquish.Evaluate(time) / 2), startSize.y - CurveSquish.Evaluate(time));
            yield return new WaitForSeconds(Time.deltaTime);

        }
        Jump(time); //прыгаем после сжимания
        StartCoroutine(UnSquish()); //разжимаем
    }

    IEnumerator UnSquish()
    {
        float time = 0;
        Vector2 startSize = transform.localScale;
        while (time < 1 || transform.localScale.x < 1)
        {
            time += Time.deltaTime;
            transform.localScale = new Vector3(Mathf.MoveTowards(startSize.x, 1, CurveUnSquish.Evaluate(time)), Mathf.MoveTowards(startSize.y, 1, CurveUnSquish.Evaluate(time)));
            yield return new WaitForSeconds(Time.deltaTime / 6);

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Contains("Spikes"))
        {

            GuiController.Instance.GameOver();
            Destroy(charBody);
            Destroy(this);

        } 
    }
}
