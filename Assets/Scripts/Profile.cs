﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Profile
{
    public int Score
    {
        get
        {
            if (!PlayerPrefs.HasKey("Score"))
            {
                PlayerPrefs.SetInt("Score", 1);
                Save();
            }
            return PlayerPrefs.GetInt("Score");
        }
        set
        {
            PlayerPrefs.SetInt("Score", value);
            Save();
        }
    }
   
    void Save()
    {
        PlayerPrefs.Save();
    }

    static Profile _instance;
    public static Profile Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new Profile();
            }
            return _instance;
        }
    }
}
