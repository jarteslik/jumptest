﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public GameObject PrefabPlatform;
    public int PlatfromCount = 0;

    public void AddPlatform(Vector2 lastPos)
    {
        PlatfromCount++;
        GameObject newPlatform = Instantiate(PrefabPlatform);
        newPlatform.transform.position = new Vector3(0, lastPos.y + 10, 0);
        StartCoroutine(BeatifulMove(newPlatform));
    }

    IEnumerator BeatifulMove(GameObject platform)
    {
        float oldPosY = platform.transform.position.y;
        float newPosY = platform.transform.position.y - Random.Range(6.5f, 8.5f);
        float time = 0;
        while (time < 1)
        {
            platform.transform.position = new Vector3(0, Mathf.Lerp(oldPosY, newPosY, time));
            time += Time.deltaTime;
            yield return new WaitForSeconds(Time.deltaTime/4);
        }
    }

    static LevelController instance;
    public static LevelController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LevelController>();
            }
            return instance;
        }
    }
}
