﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    [SerializeField]
    Transform Character;

   

    public void StartMoveCam()
    {
        Vector2 tempPos = Character.position;
        StartCoroutine(MoveCam(tempPos));
    }

    IEnumerator MoveCam(Vector2 pos)
    {
        Vector2 currentPos = transform.position;
        print("поз камеры " + currentPos.y + " и " + pos.y );
        float time = 0;
        while (time < 1)
        {
            transform.position = new Vector3(currentPos.x, Mathf.Lerp(currentPos.y,pos.y+2,time), -10);
            time += Time.deltaTime*2;
            yield return new WaitForSeconds(Time.deltaTime/10);
        }
    }

    static CamFollow instance;
    public static CamFollow Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CamFollow>();
            }
            return instance;
        }
    }
}
