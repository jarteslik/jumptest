﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour
{
    TextMesh platformCount;
    int touchCount;

    private void Start()
    {
        platformCount = GetComponentInChildren<TextMesh>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print(collision.contacts[0].normal);
        if (collision.gameObject.tag.Contains("Char/Fall"))
        {
            if (collision.contacts[0].normal.y < 0.5f) touchCount++;

            if (touchCount == 1)
            {
                CamFollow.Instance.StartMoveCam();
                LevelController.Instance.AddPlatform(transform.position);
                platformCount.text = LevelController.Instance.PlatfromCount.ToString();
                Invoke("SlowDown", 0.1f);
            }

            if (touchCount == 2) Drop();
        }
    }

    void SlowDown()
    {
        StartCoroutine(moveDown());
    }

    IEnumerator moveDown()
    {
        while (touchCount == 1)
        {
            yield return new WaitForSeconds(Time.deltaTime*2);
            transform.position -= new Vector3(0, Time.deltaTime, 0);
        }
    }

    void Drop()
    {
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        GetComponent<Rigidbody2D>().mass = 2f;
        GetComponent<Rigidbody2D>().drag = 0;
    }
}
