﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextRenderer : MonoBehaviour
{
    [ContextMenu("SetRenderer")]
    void SetRenderer()
    {
        GetComponent<Renderer>().sortingLayerName = "main";
        GetComponent<Renderer>().sortingOrder = 1;
    }
}
